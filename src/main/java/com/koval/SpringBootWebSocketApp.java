package com.koval;



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootWebSocketApp {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootWebSocketApp.class, args);
    }

}