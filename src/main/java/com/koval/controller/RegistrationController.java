package com.koval.controller;

import com.koval.data.entity.User;
import com.koval.service.RoomService;
import com.koval.service.UserService;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Locale;

@Controller
public class RegistrationController {

    private MessageSource messageSource;
    private UserService userService;
    private RoomService roomService;

    public RegistrationController(MessageSource messageSource, UserService userService, RoomService roomService) {
        this.messageSource = messageSource;
        this.userService = userService;
        this.roomService = roomService;
    }

    @GetMapping("/registration")
    public String registration() {
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(User user, Model model) {
        User createUser = userService.createUser(user.getUsername(), user.getPassword(), roomService.findById(1L));
        if (createUser == null) {
            model.addAttribute("message", messageSource.getMessage("user.registration.userExist", new Object[]{user.getUsername()}, Locale.getDefault()));
        }
        return "redirect:/login";
    }
}
