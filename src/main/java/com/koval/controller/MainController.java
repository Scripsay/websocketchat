package com.koval.controller;


import com.koval.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {

    @Autowired
    private UserService userService;

    @RequestMapping("/")
    public String index(Authentication authentication, Model model) {
        String username = authentication.getName();
        model.addAttribute("username", username);
        model.addAttribute("rooms", userService.findByUsername(username).getRooms());
        return "navbar";
    }

    @RequestMapping(path = "/login", method = RequestMethod.GET)
    public String showLoginPage() {

        return "login";

    }
}
