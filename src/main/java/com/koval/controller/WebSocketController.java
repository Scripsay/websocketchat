package com.koval.controller;


import com.koval.data.entity.Message;
import com.koval.data.repository.MessageRepo;
import com.koval.model.ChatMessage;
import com.koval.service.CommandService;
import com.koval.service.RoomService;
import com.koval.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Controller
public class WebSocketController {

    @Autowired
    private MessageRepo messageRepo;
    @Autowired
    private CommandService commmandService;
    @Autowired
    private UserService userService;
    @Autowired
    private RoomService roomService;


    @MessageMapping("/chat.sendMessage")
    @SendTo("/topic/publicChatRoom")
    public ChatMessage sendMessage(@Payload ChatMessage chatMessage) {

        if (chatMessage.getContent().startsWith("//")) {
            commmandService.executeCommand(chatMessage.getContent(), userService.findByUsername(chatMessage.getSender()));
            return null;
        }
        Message message = new Message();
        message.setPayload(chatMessage.getContent());
        message.setMsgDatetime(new Date());
        message.setRoomMessage(roomService.findById(chatMessage.getRoomId()));
        messageRepo.save(message);

        return chatMessage;
    }

    @MessageMapping("/chat.addUser")
    @SendTo("/topic/publicChatRoom")
    public ChatMessage addUser(@Payload ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) {
        //add username in web socket session
        headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
        return chatMessage;
    }
}

