package com.koval.model.commands;

import com.koval.data.entity.Room;
import com.koval.data.entity.User;
import com.koval.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class RoomRemoveCommand extends AbstractCommand{

    @Autowired
    RoomService roomService;

    @Override
    public void doCommandAction(User user) {
        List<String> args = getArgs();
        if (!args.isEmpty() && args.get(0) != null) {
            roomService.removeRoom(args.get(0), user);
        }
    }
}
