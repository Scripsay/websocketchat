package com.koval.model.commands;

import com.koval.data.entity.Room;
import com.koval.data.entity.User;
import com.koval.service.RoomService;
import com.koval.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class RoomDisconnectCommand extends AbstractCommand {

    @Autowired
    RoomService roomService;
    @Autowired
    UserService userService;

    @Override
    public void doCommandAction(User user) {
        List<String> args = getArgs();
        User disconUser = user;
        if (!args.isEmpty() && args.get(0) != null) {
            Room room = roomService.getRoomByName(args.get(0));
            if(args.size() >= 3  && "-l".equalsIgnoreCase(args.get(1))){
                if(args.get(2) != null  && (disconUser.getRoles().contains(userService.getAdminRole()) || room.getOwner().equals(user))){
                    disconUser = userService.findByUsername(args.get(2));
                    if(disconUser == null) {
                        return;
                    }
                }  else {
                    return;
                }
            }
           roomService.disconnectRoom(room, disconUser);
        }
    }
}
