package com.koval.model.commands;

import com.koval.data.entity.User;
import com.koval.service.RoomService;

import java.util.List;

public abstract class AbstractCommand {

    private List<String> args;

    public AbstractCommand() {

    }

    public abstract void doCommandAction(User user);

    public List<String> getArgs() {
        return args;
    }

    public void setArgs(List<String> args) {
        this.args = args;
    }
}
