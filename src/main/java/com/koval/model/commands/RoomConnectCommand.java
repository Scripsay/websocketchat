package com.koval.model.commands;

import com.koval.data.entity.Room;
import com.koval.data.entity.User;
import com.koval.service.RoomService;
import com.koval.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class RoomConnectCommand extends AbstractCommand {

    @Autowired
    RoomService roomService;
    @Autowired
    UserService userService;

    @Override
    public void doCommandAction(User user) {
        List<String> args = getArgs();
        User joinedUser = user;
        if (!args.isEmpty() && args.get(0) != null) {
            Room room = roomService.getRoomByName(args.get(0));
            if(args.size() >= 3  && "-l".equalsIgnoreCase(args.get(1))){
                if(args.get(2) != null  && (joinedUser.getRoles().contains(userService.getAdminRole()) || room.getOwner().equals(user))){
                    joinedUser = userService.findByUsername(args.get(2));
                   if(joinedUser == null) {
                       return;
                   }
                }
            }
            roomService.connectRoom(room, joinedUser);
        }
    }
}
