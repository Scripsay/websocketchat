package com.koval.model.commands;

import com.koval.data.entity.User;
import com.koval.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserBanCommand extends AbstractCommand {

    @Autowired
    RoomService roomService;

    @Override
    public void doCommandAction(User user) {

    }
}
