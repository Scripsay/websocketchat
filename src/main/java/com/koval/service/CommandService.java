package com.koval.service;

import com.koval.data.entity.User;
import com.koval.model.commands.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
public class CommandService {

    private static Map<String, AbstractCommand> commands;
    private static Map<String, AbstractCommand> userCommands;

    private RoomCreateCommand roomCreateCommand;
    private RoomConnectCommand roomConnectCommand;
    private RoomDisconnectCommand roomDisconnectCommand;
    private RoomRemoveCommand roomRemoveCommand;
    private RoomRenameCommand roomRenameCommand;
    private UserBanCommand userBanCommand;
    private UserModerCommand userModerCommand;
    private UserRenameCommand userRenameCommand;

    @Autowired
    public CommandService(RoomCreateCommand roomCreateCommand,
                          RoomConnectCommand roomConnectCommand,
                          RoomDisconnectCommand roomDisconnectCommand,
                          RoomRemoveCommand roomRemoveCommand,
                          RoomRenameCommand roomRenameCommand,
                          UserBanCommand userBanCommand,
                          UserModerCommand userModerCommand,
                          UserRenameCommand userRenameCommand,
                          RoomService roomService) {
        this.roomCreateCommand = roomCreateCommand;
        this.roomConnectCommand = roomConnectCommand;
        this.roomDisconnectCommand = roomDisconnectCommand;
        this.roomRemoveCommand = roomRemoveCommand;
        this.roomRenameCommand = roomRenameCommand;
        this.userBanCommand = userBanCommand;
        this.userModerCommand = userModerCommand;
        this.userRenameCommand = userRenameCommand;
        this.roomService = roomService;
    }

    @Autowired
    private RoomService roomService;

    @PostConstruct
    public void init() {
        commands = new HashMap<>();
        commands.put("create", roomCreateCommand);
        commands.put("remove", roomRemoveCommand);
        commands.put("rename", roomRenameCommand);
        commands.put("connect", roomConnectCommand);
        commands.put("disconnect", roomDisconnectCommand);
        userCommands = new HashMap<>();
        userCommands.put("rename", userRenameCommand);
        userCommands.put("ban", userBanCommand);
        userCommands.put("moderator", userModerCommand);
    }

    public void executeCommand(String message, User user) {
        AbstractCommand command = findCommand(message);
        command.doCommandAction(user);
    }

    private AbstractCommand findCommand(String message) {
        String[] words = message.split(" ");
        AbstractCommand abstractCommand = null;
        if (words[0].contains("room")) {
            abstractCommand = commands.get(words[1]);
            if (abstractCommand != null) {
                List<String> args = new ArrayList<>(Arrays.asList(words).subList(2, words.length));
                abstractCommand.setArgs(args);
            } else if (words[0].contains("user")) {
                abstractCommand = userCommands.get(words[1]);
                if (abstractCommand != null) {
                    List<String> args = new ArrayList<>(Arrays.asList(words).subList(2, words.length));
                    abstractCommand.setArgs(args);
                }
            }
        }
        return abstractCommand;
    }
}
