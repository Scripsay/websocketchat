package com.koval.service;

import com.koval.data.entity.Message;
import com.koval.data.entity.User;
import com.koval.data.repository.MessageRepo;
import com.koval.data.repository.UserRepo;
import com.koval.dto.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MessageDetailsService {


    @Autowired
    private MessageRepo messageRepo;

    public void saveMessage(Message message) {
        messageRepo.save(message);
    }
}
