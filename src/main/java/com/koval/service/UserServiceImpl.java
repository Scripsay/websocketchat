package com.koval.service;

import com.koval.data.entity.Role;
import com.koval.data.entity.Room;
import com.koval.data.entity.User;
import com.koval.data.repository.RoleRepo;
import com.koval.data.repository.RoomRepo;
import com.koval.data.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    private UserRepo userRepo;
    private PasswordEncoder passwordEncoder;
    private RoleRepo roleRepo;
   // private RoomService roomService;


    public UserServiceImpl(UserRepo userRepo, PasswordEncoder passwordEncoder, RoleRepo roleRepo) {
        this.userRepo = userRepo;
        this.passwordEncoder = passwordEncoder;
        this.roleRepo = roleRepo;
    }

    @Override
    public User createUser(String username, String password, Room room) {
        User userFromDb = userRepo.findByUsername(username);

        if (userFromDb != null) {
            return null;
        }

        User user = new User();
        user.setUsername(username);
        user.setPassword(passwordEncoder.encode(password));
        Set<Role> roles = new HashSet<>();
        roles.add(roleRepo.getOne(1L));
        user.setRoles(roles);
        user.getRooms().add(room);

        return userRepo.save(user);
    }

    @Override
    public User save(User user) {
        return userRepo.save(user);
    }

    @Override
    public User findByUsername(String username) {
        return userRepo.findByUsername(username);
    }

    @Override
    public Role getUserRole(String roleName) {
        return roleRepo.findByRoleName(roleName);
    }

    @Override
    public Role getAdminRole() {
        return roleRepo.findByRoleName("Admin");
    }

    @Override
    public Role getModerRole() {
        return roleRepo.findByRoleName("Moderator");
    }
}

