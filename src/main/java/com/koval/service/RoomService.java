package com.koval.service;


import com.koval.data.entity.Room;
import com.koval.data.entity.User;
import com.koval.data.repository.RoomRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class RoomService {

    @Autowired
    private RoomRepo roomRepo;
    @Autowired
    private UserService userService;
    @Autowired
    private HttpServletRequest request;


    public Room createNewRoom(String name, User owner) {
        Room roomFromDb = roomRepo.findByName(name);

        if (roomFromDb != null) {
            return null;
        }

        Room room = new Room();
        room.setName(name);
        room.setOwner(owner);

        return roomRepo.save(room);
    }

    public void removeRoom(String roomName, User user) {
        Room roomFromDb = roomRepo.findByName(roomName);
        if(roomFromDb != null){
            roomRepo.deleteById(roomFromDb.getId());
        }
    }

    public void connectRoom(Room roomObj, User user ){
        if (roomObj != null) {
            user.getRooms().add(roomObj);
        }
        userService.save(user);
    }

    public void disconnectRoom(Room roomObj, User user) {
        if (roomObj != null) {
            Set<Room> userRooms = user.getRooms().stream().filter(room -> !room.equals(roomObj)).collect(Collectors.toSet());
            user.setRooms(userRooms);
        }
        userService.save(user);

    }

    public Room renameRoom(String nameOld, String nameNew, User owner) {
        Room roomFromDb = roomRepo.findByName(nameOld);
        if (roomFromDb != null) {
            roomFromDb.setName(nameNew);
        }
        return roomRepo.save(roomFromDb);
    }

    public Room getRoomByName(String name){

        return roomRepo.findByName(name);
    }
    public Room findById(Long id) {
        return roomRepo.getOne(id);
    }

    public List<Room> findAllByOwnerName(String ownerName) {
        return roomRepo.findAllByOwner(ownerName);
    }
}
