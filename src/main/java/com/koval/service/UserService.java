package com.koval.service;

import com.koval.data.entity.Role;
import com.koval.data.entity.Room;
import com.koval.data.entity.User;

public interface UserService {

    User createUser(String username, String password, Room room);
    User save(User user);
    User findByUsername(String username);
    Role getUserRole(String roleName);
    Role getAdminRole();
    Role getModerRole();







}
