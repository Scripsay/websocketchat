package com.koval.service;

import com.koval.data.entity.User;
import com.koval.model.commands.AbstractCommand;

public interface SecurityService {
    boolean checkAction(AbstractCommand abstractCommand, User user);
}
