package com.koval.data.repository;


import com.koval.data.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepo extends JpaRepository<Role,Long> {
        Role findByRoleName(String roleName);
}
