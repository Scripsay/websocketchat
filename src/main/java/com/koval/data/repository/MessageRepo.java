package com.koval.data.repository;

import com.koval.data.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface MessageRepo extends JpaRepository<Message, Long> {

}
