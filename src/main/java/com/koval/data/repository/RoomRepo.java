package com.koval.data.repository;

import com.koval.data.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RoomRepo extends JpaRepository<Room,Long> {
    Room findByName(String name);
    List<Room> findAllByOwner(String ownerName);

}
