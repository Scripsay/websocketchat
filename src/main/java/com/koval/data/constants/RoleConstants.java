package com.koval.data.constants;

import com.koval.data.entity.Role;

public class RoleConstants {

    private RoleConstants() {}

    public static final Role USER = new Role(1L, "USER");
    public static final Role ADMIN = new Role(2L, "ADMIN");
    public static final Role MODERATOR = new Role(3L, "MODER");
    public static final Role OWNERROOM = new Role(4L, "OWNERROOM");



}
