package com.koval.data.entity;

public enum  State {
    ACTIVE, BANNED, DELETED;
}
