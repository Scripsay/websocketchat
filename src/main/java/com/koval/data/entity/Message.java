package com.koval.data.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer id;
    @Column(name = "user_from_id")
    private Integer userFromId;
    @Column
    private String payload;
    @Column
    private Date msgDatetime;
    @ManyToOne
    @JoinColumn(name = "room_message")
    Room roomMessage;

    public Message() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserFromId() {
        return userFromId;
    }

    public void setUserFromId(Integer userFromId) {
        this.userFromId = userFromId;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public Date getMsgDatetime() {
        return msgDatetime;
    }

    public void setMsgDatetime(Date msgDatetime) {
        this.msgDatetime = msgDatetime;
    }

    public Room getRoomMessage() {
        return roomMessage;
    }

    public void setRoomMessage(Room roomMessage) {
        this.roomMessage = roomMessage;
    }
}


