package com.koval.data.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "role")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;
    @Column(name = "role_name")
    private String roleName;



    public Role() {
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Role)){
            return false;
        }
        Role role = (Role)obj;
        return this.id.equals(role.getId());
    }

    public Role(Long id, String roleName) {
        this.id = id;
        this.roleName = roleName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }


}

